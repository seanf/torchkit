import setuptools

from torchkit import __version__

with open('requirements.txt', 'rt') as f:
    install_requires = [l.strip() for l in f.readlines()]

setuptools.setup(
    name="torchkit",
    version=__version__,
    author="Sean Fitzgibbon",
    author_email="sean.fitzgibbon@ndcn.ox.ac.uk",
    description="Boiler-plate code and utilities for working with pyTorch",
    url="https://git.fmrib.ox.ac.uk/seanf/torchkit",
    install_requires=install_requires,
    packages=['torchkit'],
    scripts=['torchkit/report.py'],
    include_package_data=True,
    python_requires='>=3.8',
    package_dir={'torchkit': 'torchkit'},
)
