#!/usr/bin/env python
import numpy as np
import torch
from torch.utils.data import Dataset, Subset
from sklearn import model_selection


class DictDataset(Dataset):
    def __init__(self, data, target, group=None, transform=None):
        self.data = data
        self.target = target
        self.group = group
        self.transform = transform

    def __getitem__(self, index):

        X = {
            k: torch.Tensor(np.atleast_1d(v[index, ...])) for k, v in self.data.items()
        }
        y = self.target[index, ...]
        # g = torch.Tensor(np.atleast_1d(self.group[index, ...])) if self.group is not None else None

        if self.transform is not None:
            raise NotImplementedError("Transforms are not yet implemented in DictDataset.")

        return X, y

    def __len__(self):
        return self.target.shape[0]

    @property
    def target(self):
        return self._target

    @target.setter
    def target(self, t):
        self._target = torch.Tensor(np.atleast_1d(t))

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, g):
        self._group = g if g is None else np.atleast_1d(g)


class DictSubset(Subset):
    @property
    def target(self):
        return self.dataset.target[self.indices]

    @property
    def group(self):
        return (
            self.dataset.group[self.indices] if self.dataset.group is not None else None
        )


def train_test_split(
    dataset: DictDataset,
    train_size: float = 0.8,
    stratified: bool = True,
    grouped: bool = False,
    shuffle: bool = False,
):

    n_splits = np.round(1 / (1 - train_size)).astype(int)

    if stratified and grouped:
        cv = model_selection.StratifiedGroupKFold(n_splits=n_splits, shuffle=shuffle)
    elif stratified and (not grouped):
        cv = model_selection.StratifiedKFold(n_splits=n_splits, shuffle=shuffle)
    elif (not stratified) and grouped:
        cv = model_selection.GroupKFold(n_splits=n_splits, shuffle=shuffle)
    else:
        cv = model_selection.KFold(n_splits=n_splits, shuffle=shuffle)

    train_idx, test_idx = next(
        cv.split(dataset.target, np.argmax(dataset.target, axis=1), dataset.group)
    )

    return DictSubset(dataset, train_idx), DictSubset(dataset, test_idx)
