#!/usr/bin/env python
from sklearn import metrics
import numpy as np
from sklearn.utils.multiclass import type_of_target


def calculate_metrics(y_true, y_pred, m=None):

    if m is None:
        m = [
            metrics.confusion_matrix,
            metrics.balanced_accuracy_score,
            metrics.matthews_corrcoef,
            metrics.accuracy_score,
        ]

        if type_of_target(y_true) == "binary":
            m.extend(
                [
                    metrics.roc_auc_score,
                    true_negative_rate,
                    true_positive_rate,
                    fix_weighted,
                ]
            )

    return {f.__name__: f(y_true, y_pred) for f in m}


def fix_metric(cm: np.ndarray):
    """
    Calculate:
    1. TNR from CM (negative==noise)
    2. TPR from CM (positive==signal)
    3. weighted TPR and TNR metric [ (3*TPR+TNR)/4 ]


    Args:
        cm: confusion matrix [true x predicted] (sklearn definition)

    Returns:
        TPR: true positive (signal) rate
        TNR: true negative (noise) rate
        weighted: weighted TPR and TNR metric [ (3*TPR+TNR)/4 ]

    """

    TPR = __tpr(cm)
    TNR = __tnr(cm)

    return TPR, TNR, (3 * TPR + TNR) / 4


def true_positive_rate(y_true, y_pred):
    cm = metrics.confusion_matrix(y_true, y_pred)
    return __tpr(cm)


def true_negative_rate(y_true, y_pred):
    cm = metrics.confusion_matrix(y_true, y_pred)
    return __tnr(cm)


def fix_weighted(y_true, y_pred):
    TPR = true_positive_rate(y_true, y_pred)
    TNR = true_negative_rate(y_true, y_pred)
    return (3 * TPR + TNR) / 4


def __tnr(cm):
    tn, fp, fn, tp = cm.ravel()

    if (tn + fp) == 0:
        tnr = 0
    else:
        tnr = tn / (tn + fp)

    return tnr


def __tpr(cm):
    tn, fp, fn, tp = cm.ravel()

    if (tp + fn) == 0:
        tpr = 0
    else:
        tpr = tp / (tp + fn)

    return tpr