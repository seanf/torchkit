#!/usr/bin/env python
import torch.nn as nn


class MonteCarloDropout(nn.Dropout):

    def forward(self, *args, **kwargs):
        self.train()
        return super().forward(*args, **kwargs)
