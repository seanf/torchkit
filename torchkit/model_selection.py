from sklearn import model_selection as skl_model_selection
import logging
from torchkit import util, scorer, data

import numpy as np

import os.path as op
import copy

import pickle


def cross_validate(clf, dataset, cv=None, checkpoint_path=None):

    if cv is None:
        if dataset.group is None:
            cv = skl_model_selection.StratifiedKFold(n_splits=5)
        else:
            cv = skl_model_selection.StratifiedGroupKFold(n_splits=5)

    logging.info(cv)

    timer = util.Timer()

    results = scorer.CvScorer()

    n_splits = cv.get_n_splits(
        dataset.target, np.argmax(dataset.target, axis=1), dataset.group
    )

    for fold_idx, (train_idx, val_idx) in enumerate(
        cv.split(dataset.target, np.argmax(dataset.target, axis=1), dataset.group)
    ):

        timer.reset()

        logging.info(f"CV: fold {fold_idx+1} of {n_splits}")

        train_dataset = data.DictSubset(dataset, train_idx)
        val_dataset = data.DictSubset(dataset, val_idx)

        logging.info(f'Train/validate instances: {len(train_dataset)}/{len(val_dataset)}')

        # clf0 = copy.deepcopy(clf)
        clf0 = pickle.loads(pickle.dumps(clf))

        logging.info(f'\n{clf0}')

        if checkpoint_path is not None:
            clf0.checkpoint_filename = op.join(
                checkpoint_path, f"checkpoint_fold{fold_idx}"
            )

        clf0.fit(train_dataset)

        pred0, actual0 = clf0.predict(val_dataset)

        results(actual0, pred0, clf0._scorer, clf0.model.state_dict())

        if checkpoint_path is not None:
            results.save(op.join(checkpoint_path, "cv_results"))

        logging.info(f"Stop: fold {fold_idx+1} of {n_splits} ({timer.elapsed():0.2f}s)")

        del clf0, train_dataset, val_dataset

    return results
