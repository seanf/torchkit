# from msilib.schema import SelfReg
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, WeightedRandomSampler
from torch.autograd import Variable

# import os.path as op

from typing import Optional, Union
import logging

import numpy as np
from sklearn import model_selection

from torchkit.data import DictDataset, DictSubset
from torchkit import util, metrics, scorer

from tabulate import tabulate


class NeuralNetwork:
    def __init__(
        self,
        model: nn.Module,
        optimiser: Optional[optim.Optimizer] = None,
        loss_function: nn.Module = nn.BCELoss(),
        batch_size: int = 32,
        learning_rate: float = 1e-6,
        # learning_rate_scheduler: Optional[nn.Module] = None,
        weight_decay: float = 0,
        max_iter: int = 100,
        device: Optional[str] = None,
        do_early_stopping=False,
        do_validation=False,
        validation_fraction=0.2,
        patience: int = 10,
        # checkpoint_filename: Optional[str] = None,
        do_oversampling: bool = False,
        montecarlo_repeats: Optional[int] = None,
    ):

        self.model = model
        self.loss_function = loss_function
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        # self.learning_rate_scheduler = learning_rate_scheduler
        self.weight_decay = weight_decay
        self.max_iter = max_iter
        self.patience = patience
        self.do_early_stopping = do_early_stopping
        self.validation_fraction = validation_fraction
        self.do_validation = do_validation
        # self.checkpoint_filename = checkpoint_filename
        self.do_oversampling = do_oversampling
        self.montecarlo_repeats = montecarlo_repeats

        if optimiser is None:
            optimiser = optim.Adam(
                list(model.parameters()),
                lr=self.learning_rate,
                weight_decay=self.weight_decay,
            )
        self.optimiser = optimiser

        if device is None:
            device = "cuda" if torch.cuda.is_available() else "cpu"
        self.device = device

    def fit(self, dataset: DictDataset):

        if self.do_early_stopping:
            self.do_validation = True

        if self.do_validation:

            n_splits = np.round(1 / self.validation_fraction).astype(int)

            if dataset.group is not None:
                cv = model_selection.StratifiedGroupKFold(n_splits=n_splits)
            else:
                cv = model_selection.StratifiedKFold(n_splits=n_splits)

            logging.info(f"validation splitter: {cv}")

            train_idx, val_idx = next(
                cv.split(
                    dataset.target, np.argmax(dataset.target, axis=1), dataset.group
                )
            )

            train_dataset = DictSubset(dataset, train_idx)
            validation_dataset = DictSubset(dataset, val_idx)

        train_dataloader = self._get_dataloader(
            train_dataset if self.do_validation else dataset,
            weighted_sampling=self.do_oversampling,
        )

        if self.do_validation:
            validation_dataloader = self._get_dataloader(
                validation_dataset,
                weighted_sampling=False,
            )

        self.model.to(self.device)
        self.loss_function.to(self.device)

        if self.do_early_stopping:
            early_stopping = util.EarlyStopping(patience=self.patience)

        timer = util.Timer()

        self._scorer = scorer.EpochScorer()

        for epoch in np.arange(self.max_iter) + 1:

            self._current_epoch = epoch

            # reset timer
            timer.reset()

            # step train
            train_scorer = self.train_step(train_dataloader)

            # step val

            if self.do_validation:
                validation_scorer = self.validate_step(validation_dataloader)
            else:
                validation_scorer = None

            self._scorer(train_scorer, validation_scorer)

            # log
            log_str = f"Epoch: {epoch}/{self.max_iter}, time: {timer.elapsed():0.2f}s, train-loss: {train_scorer.loss:0.4e}"

            if self.do_validation:
                log_str += f", validation-loss: {validation_scorer.loss:0.4e}"
            # if do_scheduler:
            #     log_str += f", Learning Rate: {self.learning_rate_scheduler.get_last_lr()[0]:0.4e}"

            logging.info(log_str)

            # step the sheduler
            # if do_scheduler:
            #     self.learning_rate_scheduler.step()

            if self.do_early_stopping and early_stopping(
                validation_scorer.loss, self.model, epoch
            ):
                self.model = early_stopping.best_model
                break

            # checkpoint
            # if self.checkpoint_filename is not None:
            #     self.checkpoint()

        # move model back to cpu
        self.model.to("cpu")

        # clear cuda cache
        torch.cuda.empty_cache()

    def predict(self, dataset: DictDataset):
        logging.info('Running prediction')
        if self.montecarlo_repeats is None:
            return self._predict(dataset)
        else:
            return self._predict_montecarlo(dataset, n_repeats=self.montecarlo_repeats)

    def _predict(self, dataset: DictDataset):

        dataloader = DataLoader(
            dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=0,
            drop_last=False,
        )

        timer = util.Timer()

        logging.info("Start")

        self.model.to(self.device)

        pred, actual = [], []

        with torch.no_grad():
            for data, label in dataloader:
                data = {k: Variable(v.to(self.device)) for k, v in data.items()}
                label = Variable(label)

                pred += [
                    self.model.forward(**data).detach().cpu().numpy().astype(float)
                ]
                actual += [label]

        # move model back to cpu
        self.model.to("cpu")

        # clear cuda cache
        torch.cuda.empty_cache()

        logging.info(f"Stop: ({timer.elapsed():0.2f}s)")

        return np.concatenate(pred, axis=0), np.concatenate(actual, axis=0)

    def _predict_montecarlo(self, dataset: DictDataset, n_repeats: int = 100):

        logging.info(f'Running montecarlo prediction with {n_repeats} repeats')

        pred = [self._predict(dataset)[0] for _ in range(n_repeats)]
        pred_proba = np.mean(np.stack(pred), axis=0)
        return pred_proba, dataset.target
    
    def score(self, dataset):

        pred, actual = self.predict(dataset)

        m0 = metrics.calculate_metrics(
            np.argmax(actual, axis=-1), np.argmax(pred, axis=-1)
        )

        return m0

    def checkpoint(self):

        fname = self.checkpoint_filename
        if not fname.endswith(".pth.tar"):
            fname += ".pth.tar"

        torch.save(
            {
                "epoch": self._current_epoch,
                "model_state_dict": self.model.state_dict(),
                "optimizer_state_dict": self.optimiser.state_dict(),
                "scorer": self._scorer,
            },
            fname,
        )

    def _get_dataloader(
        self,
        dataset: Union[DictDataset, DictSubset],
        weighted_sampling: bool = False,
        shuffle: bool = True,
    ):

        weighted_sampler = None

        if weighted_sampling:
            _y = dataset.target.numpy()

            weights = np.sum(_y, axis=0)
            weights = np.max(weights) / weights

            logging.info(f"Oversampling weights: {weights}")

            weights = weights[np.argmax(_y, axis=-1)]

            weighted_sampler = WeightedRandomSampler(
                weights=weights, num_samples=len(weights), replacement=True
            )

            shuffle = False

        dataloader = DataLoader(
            dataset,
            batch_size=self.batch_size,
            shuffle=shuffle,
            num_workers=0,
            drop_last=True,
            sampler=weighted_sampler,
        )
        return dataloader

    def train_step(self, dataloader, batch_scorer=scorer.BatchScorer()):

        self.model.train()

        for idx_batch, (data_batch, label_batch) in enumerate(dataloader):

            data_batch = {k: Variable(v.to(self.device)) for k, v in data_batch.items()}
            label_batch = Variable(label_batch.to(self.device))

            # Forward
            self.optimiser.zero_grad()
            pred_batch = self.model.forward(**data_batch)

            # Calc loss
            train_loss = self.loss_function(pred_batch, label_batch)

            # Backprop the loss
            train_loss.backward()
            self.optimiser.step()

            batch_scorer(label_batch, pred_batch, loss=train_loss)

            for d in data_batch.values():
                d.to("cpu")
            label_batch.to("cpu")

            del data_batch, label_batch

        return batch_scorer

    def validate_step(self, dataloader, batch_scorer=scorer.BatchScorer()):

        self.model.eval()

        with torch.no_grad():
            for idx_batch, (data_batch, label_batch) in enumerate(dataloader):

                data_batch = {
                    k: Variable(v.to(self.device)) for k, v in data_batch.items()
                }
                label_batch = Variable(label_batch.to(self.device))

                pred_batch = self.model.forward(**data_batch)
                validation_loss = self.loss_function(pred_batch, label_batch)

                batch_scorer(label_batch, pred_batch, loss=validation_loss)

                for d in data_batch.values():
                    d.to("cpu")
                label_batch.to("cpu")

                del data_batch, label_batch

        return batch_scorer

    def __str__(self):

        total_params = sum(p.numel() for p in self.model.parameters())
        total_trainable_params = sum(
            p.numel() for p in self.model.parameters() if p.requires_grad
        )

        mc_repeats = 'None' if self.montecarlo_repeats is None else self.montecarlo_repeats

        table = [
            ["model", self.model],
            ["total parameters", total_params],
            ["total trainable parameters", total_trainable_params],
            ["optimiser", self.optimiser],
            ["loss function", self.loss_function],
            ["batch size", self.batch_size],
            ["learning rate", self.learning_rate],
            ["weight decay", self.weight_decay],
            ["max iterations", self.max_iter],
            ["device", self.device],
            ["do early-stopping", self.do_early_stopping],
            ["early-stopping patience", self.patience],
            ["do validation", self.do_validation],
            ["validation fraction", self.validation_fraction],
            ["do oversampling", self.do_oversampling],
            ["montecarlo prediction repeats", mc_repeats],
        ]

        return tabulate(table, tablefmt="grid")
