#!/usr/bin/env python
import torch
from tabulate import tabulate
import pandas as pd
import click


@click.command()
@click.argument("path")
def report(path):
    d = torch.load(f'{path}/cv_results.pt')

    rpt = f'CV training results: {path}\n\n'

    rpt += tabulate(pd.DataFrame(d.score()).describe(), headers='keys', tablefmt="grid")
    rpt += '\n\nPer fold:\n\n'
    rpt += str(d)

    print(rpt)

    return rpt


if __name__ == '__main__':
    report()

# print(report('/Users/seanf/Downloads/test_results/tk'))