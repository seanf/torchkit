import numpy as np
import torch
import pandas as pd

from torchkit import metrics
from tabulate import tabulate


class Scorer:
    def __init__(self, actual, predicted):
        self._actual, self._predicted = actual, predicted

    @property
    def score(self):
        return metrics.calculate_metrics(
            np.argmax(self._actual, axis=-1), np.argmax(self._predicted, axis=-1)
        )


class BatchScorer(Scorer):
    """Model training/fitting performance metrics.  Accumulates over training batches"""

    def __init__(self):
        Scorer.__init__(self, [], [])
        self._loss = []

    def __call__(self, actual, predicted, loss=None):

        if isinstance(predicted, torch.Tensor):
            predicted = predicted.detach().cpu().numpy()
        if isinstance(actual, torch.Tensor):
            actual = actual.detach().cpu().numpy()
        if isinstance(loss, torch.Tensor):
            loss = loss.detach().cpu().numpy()

        self._actual.append(actual)
        self._predicted.append(predicted)
        if loss is not None:
            self._loss.append(loss)

    @property
    def score(self):
        actual = np.concatenate(self._actual, axis=0)
        pred = np.concatenate(self._predicted, axis=0)

        m = metrics.calculate_metrics(
            np.argmax(actual, axis=-1), np.argmax(pred, axis=-1)
        )

        return m

    @property
    def loss(self):
        return np.mean(self._loss)


class EpochScorer:
    """Accumulate scores over training epochs. Supports train and validation metrics."""

    def __init__(self):
        self._train_batch_scorer, self._val_batch_scorer = [], []

    def __call__(self, train_epoch_scorer, val_epoch_scorer=None):

        self._train_batch_scorer.append(train_epoch_scorer)
        if val_epoch_scorer is not None:
            self._val_batch_scorer.append(val_epoch_scorer)

    @property
    def train_epoch(self):
        return self._train_batch_scorer

    @property
    def validation_epoch(self):
        return self._val_batch_scorer

    @property
    def train_loss(self):
        return [s.loss for s in self._train_batch_scorer]

    @property
    def validation_loss(self):
        return [s.loss for s in self._val_batch_scorer]

    @property
    def train_score(self):
        return [s.score for s in self._train_batch_scorer]

    @property
    def validation_score(self):
        return [s.score for s in self._val_batch_scorer]


# TODO: serialise to save, or add to_dict and from_dict methods
class CvScorer(Scorer):
    """Storage for CV folds.  Accumulates over folds"""

    def __init__(self):
        Scorer.__init__(self, [], [])
        self._epoch_scorer, self._models = [], []

    def __call__(self, actual, predicted, epoch_scorer=None, model=None):

        if isinstance(predicted, torch.Tensor):
            predicted = predicted.detach().cpu().numpy()
        if isinstance(actual, torch.Tensor):
            actual = actual.detach().cpu().numpy()

        self._actual.append(actual)
        self._predicted.append(predicted)
        if epoch_scorer is not None:
            self._epoch_scorer.append(epoch_scorer)
        if model is not None:
            self._models.append(model)

    @property
    def epoch_scorer(self):
        return self._epoch_scorer

    @property
    def actual(self):
        return self._actual

    @property
    def predicted(self):
        return self._predicted

    def score(self, threshold=0.5):
        def apply_thres(x):
            return (x > threshold).astype(int)

        m = [
            metrics.calculate_metrics(
                np.argmax(actual, axis=-1), np.argmax(pred, axis=-1)
            )
            for actual, pred in zip(self._actual, self._predicted)
        ]
        return m

    def save(self, fname):

        if not fname.endswith(".pt"):
            fname += ".pt"

        torch.save(self, fname)

    def __str__(self):

        return tabulate(self.score(), headers='keys', tablefmt="grid")
