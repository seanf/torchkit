#!/usr/bin/env python
import time
import logging
import copy


def timeit(f):
    def timed(*args, **kw):
        t_start = time.time()
        print("BEGIN {}".format(f.__name__))
        result = f(*args, **kw)
        t_end = time.time()
        print("END {}: Elapsed time {:0.2f}s".format(f.__name__, t_end - t_start))
        return result

    return timed


class Timer:
    def __init__(self):
        self.reset()

    def reset(self):
        self._start = time.time()

    def elapsed(self):
        return time.time() - self._start


class EarlyStopping:
    # Early stops the training if the validation loss doesnt improve after a given patience
    def __init__(self, patience=10, verbose=False):
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self._best_score = None
        self.best_model = None
        self.best_epoch = None

    @property
    def best_score(self):
        return self._best_score

    @best_score.setter
    def best_score(self, score):
        # logging.debug(f"Early stopping new best score: {score:0.4e}")
        self._best_score = score

    def __call__(self, score, model, epoch):

        early_stop = False

        if self.best_score is None:  # initialise (only runs first time)
            self.best_score = score
            self.best_model = copy.deepcopy(model).to("cpu")
            self.best_epoch = epoch

        if score > self.best_score:  # score worse than best (going up)
            self.counter += 1
            logging.info(
                f"Epoch {epoch}: Early stopping counter ({self.counter}/{self.patience})"
            )
            # when counter exceeds patience value, activate early stopping
            if self.counter >= self.patience:
                early_stop = True
        elif score <= self.best_score:  # score better than best (going down)
            self.counter = 0
            # logging.debug(f"Epoch {epoch}: Early stopping counter reset")
            self.best_score = score
            self.best_model = copy.deepcopy(model).to("cpu")
            self.best_epoch = epoch

        return early_stop
